import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import {Header, Story, Pages, Footer} from './Index';

class App extends Component {
  render() {
    return (
      <View>
        <Header />
        <ScrollView>
          <Story />
          <Pages />
        </ScrollView>
        <Footer />
      </View>
    );
  }
}

export default App;
